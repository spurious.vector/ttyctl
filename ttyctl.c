#include <linux/module.h> 
#include <linux/kernel.h> 
#include <linux/init.h> 
#include <linux/tty.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

#define EOF -1

#define DEVNAME "ttyctl"
static int chdev_major;
static int chdev_open = 0;
static struct tty_struct* cur_tty;


static ssize_t dev_read(struct file* filep, char* buf, size_t length, loff_t* offset) {
	return -EINVAL;
}

/*
 *	Called when something tries to open the device file.
 *
 */

static int dev_open(struct inode* inode, struct file* file) {
	if (chdev_open) {
		return -EBUSY;
	}

	++chdev_open;
	try_module_get(THIS_MODULE);
	return 0;
}

/*
 *	Called when something closes the chardev file.
 *
 */


static int dev_close(struct inode* inode, struct file* file) {
	--chdev_open;
	module_put(THIS_MODULE);
	return 0;
}


static ssize_t dev_write(struct file* filp, const char* buf, size_t len, loff_t* off) {
	cur_tty = get_current_tty();
	if (strncmp(buf, "kill", 4) == 0) {
		tty_hangup(cur_tty);
	} else if (strncmp(buf, "lock", 4) == 0) {
		tty_lock(cur_tty);
	} else if (strncmp(buf, "CRAP OUT", 8) == 0) {
		struct winsize ws = cur_tty->winsize;
		ws.ws_row = 2;
		ws.ws_col = 2;
		ws.ws_xpixel = 2;
		ws.ws_ypixel = 2;
		tty_do_resize(cur_tty, &ws);
	} else {
		return -EINVAL;
	}
	
	return 0;
}


static struct file_operations fops = {
	.read = dev_read,
	.write = dev_write,
	.open = dev_open,
	.release = dev_close
};


static int __init init_mod(void)  {
	chdev_major = register_chrdev(0, DEVNAME, &fops);
	printk(KERN_INFO "TTYCTL MAJOR: %d\n", chdev_major);

	if (chdev_major < 0) {
		printk(KERN_ALERT "Could not register chrdev for ttyctl!\n");
		return chdev_major;
	}

    return 0; 
} 

static void __exit exit_mod(void)  { 
	unregister_chrdev(chdev_major, DEVNAME);
} 

module_init(init_mod); 
module_exit(exit_mod); 


MODULE_LICENSE("GPL");
